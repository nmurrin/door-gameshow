# door-gameshow

## To run backend locally for development
- -> ./gradlew bootRun

## To run frontend locally for development
- -> npm install
- -> npm run start:dev
- http://localhost:3000

## To package everything into an executable jar
- -> ./gradlew bundleJs
- -> ./gradlew bootJar
- -> java -jar build/libs/door-gameshow-0.1.0.jar
- http://localhost:8080

## To run from docker
- Have Docker installed
- -> ./gradlew bundleJs
- -> ./gradlew docker
- -> docker run -d -p 80:8080 com.norm/door-gameshow
- http://localhost
