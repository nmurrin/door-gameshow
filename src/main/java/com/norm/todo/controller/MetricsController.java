package com.norm.todo.controller;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:3001", "http://localhost:9000"})
@RestController
@RequestMapping("${spring.data.rest.base-path}")
public class MetricsController {

    private static HashMap<String, Integer> metrics = new HashMap();

    @RequestMapping("/metrics")
    public HashMap<String, Integer> getMetrics() {
        return metrics;
    }

    @RequestMapping(value = "/metrics/{metric}", method = RequestMethod.POST)
    public void saveMetric(@PathVariable("metric") String metric) {
        if (metrics.containsKey(metric)) {
            metrics.put(metric, metrics.get(metric) + 1);
        } else {
            metrics.put(metric, 1);
        }
        System.out.println("metric = " + metric);
    }

    @RequestMapping(value = "/metrics/{metric}", method = RequestMethod.GET)
    public int getMetric(@PathVariable("metric") String metric) {
        if (metrics.containsKey(metric)) {
            return metrics.get(metric);
        } else {
            return 0;
        }
    }
}
