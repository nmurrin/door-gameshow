import door1 from './Door1.jpg';
import door2 from './Door2.jpg';
import door3 from './Door3.jpg';
import door4 from './Door4.png';
import door5 from './Door5.jpg';
import door6 from './Door6.jpg';
import door7 from './Door7.jpg';
import door8 from './Door8.png';
import door9 from './Door9.jpg';
import door10 from './Door10.jpg';
import door11 from './Door11.jpg';
import door12 from './Door12.jpg';
import door13 from './Door13.jpg';
import door14 from './Door14.jpg';
import door15 from './Door15.jpg';
import door16 from './Door16.jpg';

export const doorsArray = [door1, door2, door3, door4, door5, door6, door7, door8, door9, door10, door11, door12, door13, door14, door15, door16];
