import React, { useContext, useState } from 'react';
import axios from "axios";
import GameContext from "../context/gameContext";
import { Link } from "react-router-dom";
import { doorsArray } from "../images";

const prizeDoor = () => Math.floor((Math.random() * 3) + 1);
const randomImage = () => doorsArray[Math.floor((Math.random() * 15))];
const randomDoors = () => [randomImage(), randomImage(), randomImage()];
const API_ROOT_URL = '/api';

const GAME_STATE = {
    IN_PROGRESS: "inProgress",
    WON: "won",
    LOST: "lost",
}

const METHOD = {
    STAYED: "stayed",
    SWITCHED: "switched"
}

const DoorGameshow = () => {
    const {username, setUsername, team, setTeam, usernameSubmitted, setUsernameSubmitted} = useContext(GameContext);
    const [winningDoor, setWinningDoor] = useState(prizeDoor());
    const [picked, setPicked] = useState(null);
    const [wrongDoor, setWrongDoor] = useState(null);
    const [gameState, setGameState] = useState(GAME_STATE.IN_PROGRESS);
    const [method, setMethod] = useState(null);
    const [images, setImages] = useState(randomDoors());

    const pickDoor = (pick) => {
        if (gameState !== GAME_STATE.IN_PROGRESS) {
            return;
        }

        if (picked) {
            if (pick === wrongDoor) return;  // NOP
            return finishGame(pick === picked ? METHOD.STAYED : METHOD.SWITCHED);
        }

        setPicked(pick);
        for (let x=1; x<4; x++) {
            if (x !== winningDoor && x !== pick) {
                setWrongDoor(x);
            }
        }
    }

    const finishGame = (method) => {
        const state = method === METHOD.STAYED ?
            picked === winningDoor ? GAME_STATE.WON : GAME_STATE.LOST :
            picked !== winningDoor ? GAME_STATE.WON : GAME_STATE.LOST;

        setGameState(state);
        setMethod(method);
        const gameMetric = `${method}-${state}`;
        axios.post(`${API_ROOT_URL}/metrics/${gameMetric}`);
        const userMetric = `${username}<->${state}`;
        axios.post(`${API_ROOT_URL}/metrics/${userMetric}`);
        const teamMetric = `${team}<-->${state}`;
        axios.post(`${API_ROOT_URL}/metrics/${teamMetric}`);
    }

    const resetGame = () => {
        setWinningDoor(prizeDoor());
        setPicked(null);
        setWrongDoor(null);
        setGameState(GAME_STATE.IN_PROGRESS);
        setImages(randomDoors())
    }

    const getDoorStyle = (door) => {
        const style = {
            height: 200,
            cursor: "pointer"
        };
        if (gameState === GAME_STATE.IN_PROGRESS) {
            if (door === picked) {
                style.border = "5px solid green";
            } else if (door === wrongDoor) {
                style.border = "5px solid red";
            } else if (picked !== null) {
                style.border = "5px solid yellow";
            }
        } else {
            if (door === winningDoor) {
                style.border = "5px solid green";
            } else {
                style.border = "5px solid red";
            }
        }
        return style;
    }

    const getDoorCharStyle = (door) => {
        const style = {
            fontSize: "6rem",
            cursor: "pointer",
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%"
        };
        if (gameState === GAME_STATE.IN_PROGRESS) {
            if (door === picked) {
                style.color = "green";
            } else if (door === wrongDoor) {
                style.color = "red";
            } else if (picked !== null) {
                style.color = "yellow";
            }
        } else {
            if (door === winningDoor) {
                style.color = "green";
            } else {
                style.color = "red";
            }
        }
        return style;
    }

    const getDoorChar = (door) => {
        if (gameState === GAME_STATE.IN_PROGRESS) {
            if (door === picked) {
                return '?';
            } else if (door === wrongDoor) {
                return 'X';
            } else if (picked !== null) {
                return '?';
            }
        } else {
            if (door === winningDoor) {
                return '$';
            } else {
                return 'X';
            }
        }
    }

    const renderDoor = (door) => {
        return <div style={ { textAlign: "center" } }>
            <div style={ { position: "relative", textAlign: "center" } }>
                <img alt='logo' style={ getDoorStyle(door) } onClick={ () => pickDoor(door) }
                     src={ images[door - 1] }/>
                <div style={ getDoorCharStyle(door) } onClick={ () => pickDoor(door) }>{ getDoorChar(door) }</div>
            </div>
            <br/>
            <button className={`ui ${gameState === GAME_STATE.IN_PROGRESS && door !== wrongDoor ? 'secondary' : 'basic grey'} button`} onClick={ () => pickDoor(door) }>Door { door }</button>
        </div>
    }

    const handleTeamChange = event => {
        console.log(event.target.value);
        setTeam(event.target.value);
    };

    return (
        <div className="ui container">
            <div style={ { width: "100%", marginLeft: "auto", marginRight: "auto" } }>
                <div style={ { display: "flex", justifyContent: "space-between", marginTop: "1rem" } }>
                    <Link to={`/stats`}><h3><i className="chart bar icon"/> View Statistics</h3></Link>
                    { usernameSubmitted && <span><h3><i className="icon user"></i> { username } on { team }</h3></span> }
                </div>
                <div className="ui divider"/>
                <h1>Norms Pick-A-Door Gameshow</h1>

                { !usernameSubmitted &&
                <form className="ui form">
                    <div className="field">
                        <label htmlFor="username">Enter a name:</label>
                        <input id="username" value={ username } onChange={ (e) => setUsername(e.target.value) }/>
                    </div>

                    <div className="field">
                        <label htmlFor="team">Pick your team:</label>
                        <select id="team" value={ team } onChange={ handleTeamChange }>
                            <option value="Core">Core</option>
                            <option value="Cybertron">Cybertron</option>
                            <option value="Megatron">Megatron</option>
                            <option value="Omeega Supreme">Omega Supreme</option>
                            <option value="Test Team">Test Team</option>
                            <option value="Wreckers">Wreckers</option>
                        </select>
                    </div>

                    <button className="ui button" onClick={ () => {
                        setUsernameSubmitted(true);
                    } }>Submit
                    </button>
                </form>
                }

                { usernameSubmitted &&
                <div>

                    <div className="ui divider"/>
                    <div style={ {
                        display: "flex",
                        justifyContent: "space-between",
                        flexDirection: "column",
                        minWidth: "400px",
                        maxWidth: "800px",
                        marginLeft: "auto",
                        marginRight: "auto"
                    } }>
                        <h3>Pick a door, any door - Only one door has a prize!</h3>
                        <div style={ {
                            display: "flex",
                            justifyContent: "space-between",
                            minWidth: "400px",
                            maxWidth: "800px"
                        } }>
                            { renderDoor(1) }
                            { renderDoor(2) }
                            { renderDoor(3) }
                        </div>
                    </div>
                    <div className="ui divider"/>

                    { picked && gameState === GAME_STATE.IN_PROGRESS &&
                    <div style={ { width: "100%", marginTop: "1rem" } } className="ui two column grid container">
                        <div className="ten wide column">
                            <h3>You picked door { picked }! Door { wrongDoor } is not the correct door! Do want want
                                to:</h3>
                        </div>
                        <div className="six wide column">
                            <form className="ui form">
                                <button className="ui secondary button right floated"
                                        onClick={ () => finishGame(METHOD.SWITCHED) }> SWITCH to
                                    other?
                                </button>
                                <button className="ui secondary button right floated"
                                        onClick={ () => finishGame(METHOD.STAYED) }>
                                    STAY with pick?
                                </button>
                            </form>
                        </div>
                    </div>
                    }

                    { gameState !== GAME_STATE.IN_PROGRESS &&
                    <div style={ { width: "100%", marginTop: "1rem" } } className="ui two column grid container">
                        <div className="eight wide column">
                            { gameState === 'lost' ?
                                <h3 style={ { "color": "red" } }>You { method } and LOST!</h3>
                                :
                                <h3 style={ { "color": "green" } }>You { method } and WON!</h3>
                            }
                        </div>
                        <div className="eight wide column">
                            <form className="ui form">
                                <button className="ui secondary button right floated" onClick={ resetGame }>PLAY AGAIN?
                                </button>
                            </form>
                        </div>
                    </div>
                    }

                </div>
                }

            </div>
        </div>
    )
}

export default DoorGameshow;
