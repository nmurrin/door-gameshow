import React, { useContext, useEffect, useState } from 'react';
import axios from "axios";
import { Link } from "react-router-dom";
import GameContext from "../context/gameContext";

const ROOT_URL = '/api';

const DoorStatistics = () => {
    const {username} = useContext(GameContext);
    const [stayWin, setStayWin] = useState(0);
    const [stayLose, setStayLose] = useState(0);
    const [switchWin, setSwitchWin] = useState(0);
    const [switchLose, setSwitchLose] = useState(0);
    const [metrics, setMetrics] = useState(null);

    const getMetrics = () => {
        axios.get(`${ROOT_URL}/metrics/stayed-won`).then((response) => {
            setStayWin(response.data);
        });
        axios.get(`${ROOT_URL}/metrics/stayed-lost`).then((response) => {
            setStayLose(response.data);
        });
        axios.get(`${ROOT_URL}/metrics/switched-won`).then((response) => {
            setSwitchWin(response.data);
        });
        axios.get(`${ROOT_URL}/metrics/switched-lost`).then((response) => {
            setSwitchLose(response.data);
        });
        axios.get(`${ROOT_URL}/metrics`).then((response) => {
            setMetrics(response.data);
        });
    }

    useEffect(() => {
        getMetrics();
        const interval = setInterval(getMetrics, 10000);
        return () => {
            clearInterval(interval);
        }
    }, [])

    const renderPlayerMetricsRows = () => {
        const stats = [];
        const keys = Object.keys(metrics);

        keys.forEach((key) => {
            if (!key.includes("switched") &&
                !key.includes("stayed") &&
                key.includes("won") &&
                key.includes("<->")
            ) {
                const user = key.split("<->")[0];
                const winKey = key;
                const loseKey = user + "<->lost";
                const wins = metrics[winKey];
                const losses = loseKey in metrics ? metrics[loseKey] : 0;
                if (wins + losses > 9) {
                    const winPercent = (wins / (wins + losses) * 100).toFixed(2);
                    stats.push({user, winPercent});
                }
            }
        })

        stats.sort((a, b) => {
            return b.winPercent - a.winPercent;
        })

        return stats.map((stat) => {
            return (
                <tr key={stat.user}>
                    <td data-label="User">{ stat.user }</td>
                    <td data-label="Percent">{ stat.winPercent }</td>
                </tr>
            );
        });
    }

    const renderTeamMetricsRows = () => {
        const stats = [];
        const keys = Object.keys(metrics);

        keys.forEach((key) => {
            if (!key.includes("switched") &&
                !key.includes("stayed") &&
                key.includes("won") &&
                key.includes("<-->")
            ) {
                const team = key.split("<-->")[0];
                const winKey = key;
                const loseKey = team + "<-->lost";
                const wins = metrics[winKey];
                const losses = loseKey in metrics ? metrics[loseKey] : 0;
                if (wins + losses > 9) {
                    const winPercent = (wins / (wins + losses) * 100).toFixed(2);
                    stats.push({team, winPercent});
                }
            }
        })

        stats.sort((a, b) => {
            return b.winPercent - a.winPercent;
        })

        return stats.map((stat) => {
            return (
                <tr key={stat.team}>
                    <td data-label="Team">{ stat.team }</td>
                    <td data-label="Percent">{ stat.winPercent }</td>
                </tr>
            );
        });
    }

    const getStrategyPercent = (wins, losses) => {
        if (wins == 0 || (wins + losses) == 0) {
            return 0;
        } else {
            return (wins / (wins + losses) * 100).toFixed(2);
        }
    }

    return (
        <div className="ui container">
            <div style={ { width: "100%", marginLeft: "auto", marginRight: "auto" } }>
                <div style={ { display: "flex", justifyContent: "space-between", marginTop: "1rem" } }>
                    <Link to={`/`}><h3><i className="smile icon"/>Back to Game</h3></Link>
                    <span><h3><i className="icon user"></i>Name: { username }</h3></span>
                </div>
                <div className="ui divider"/>
                <h1>Game Statistics</h1>
                <div className="ui divider"/>

                <div style={ {
                    display: "flex",
                    justifyContent: "space-between",
                    minWidth: "400px",
                    maxWidth: "800px",
                    marginLeft: "auto",
                    marginRight: "auto"
                } }>
                    <div>
                        <h2>Strategy Statistics</h2>
                        <table className="ui celled table">
                            <thead>
                            <tr>
                                <th>Strategy</th>
                                <th>Won</th>
                                <th>Lost</th>
                                <th>Percent</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td data-label="Strategy">Stayed</td>
                                <td data-label="Won">{ stayWin }</td>
                                <td data-label="Lost">{ stayLose }</td>
                                <td data-label="Percent">{ getStrategyPercent(stayWin, stayLose) }</td>
                            </tr>
                            <tr>
                                <td data-label="Strategy">Switched</td>
                                <td data-label="Won">{ switchWin }</td>
                                <td data-label="Lost">{ switchLose }</td>
                                <td data-label="Percent">{ getStrategyPercent(switchWin, switchLose) }</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h2>Top Players</h2>
                        <table className="ui celled table">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Win Percent</th>
                            </tr>
                            </thead>
                            <tbody>
                            { metrics && renderPlayerMetricsRows() }
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <h2>Team Rankings</h2>
                        <table className="ui celled table">
                            <thead>
                            <tr>
                                <th>Team</th>
                                <th>Win Percent</th>
                            </tr>
                            </thead>
                            <tbody>
                            { metrics && renderTeamMetricsRows() }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DoorStatistics;
