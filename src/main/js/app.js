import React, {useState} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import GameContext from "./context/gameContext";
import DoorStatistics from "./components/doorStatistics";
import DoorGameshow from "./components/doorGameshow";

const App = () => {

    const [username, setUsername] = useState("");
    const [team, setTeam] = useState("Core");
    const [usernameSubmitted, setUsernameSubmitted] = useState(null);

    return (
        <BrowserRouter>
            <div>
                <Switch>
                    <GameContext.Provider value={{username, setUsername, team, setTeam, usernameSubmitted, setUsernameSubmitted}}>
                        <Route exact path="/stats" component={DoorStatistics} />
                        <Route exact path="/" component={DoorGameshow} />
                    </GameContext.Provider>
                </Switch>
            </div>
        </BrowserRouter>
    )
}

export default App;
