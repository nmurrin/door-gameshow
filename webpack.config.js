const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/main/js/index.js'
  },
  output: {
    path: path.join(__dirname, 'src/main/resources/static'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }, {
      test: /\.(jpg|png)$/,
      use: {
        loader: 'url-loader',
      },
    },]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './src/main/resources/static',
    port: 3000,
    proxy: [{
      context: ['/api'],
      target: 'http://localhost:8080',
    }]
  }
};
